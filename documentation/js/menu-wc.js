'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">nestjs-graphql-server documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CompanyModule.html" data-type="entity-link" >CompanyModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-CompanyModule-7f352bc1ea1f1dda957c4f2a5ab6bed22f3f1f290eaa258d4c153742633ea5529513a95636da3d6c30d3b6cae66aa81c4e170fa26abd148c8a650dd863140f5c"' : 'data-bs-target="#xs-injectables-links-module-CompanyModule-7f352bc1ea1f1dda957c4f2a5ab6bed22f3f1f290eaa258d4c153742633ea5529513a95636da3d6c30d3b6cae66aa81c4e170fa26abd148c8a650dd863140f5c"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CompanyModule-7f352bc1ea1f1dda957c4f2a5ab6bed22f3f1f290eaa258d4c153742633ea5529513a95636da3d6c30d3b6cae66aa81c4e170fa26abd148c8a650dd863140f5c"' :
                                        'id="xs-injectables-links-module-CompanyModule-7f352bc1ea1f1dda957c4f2a5ab6bed22f3f1f290eaa258d4c153742633ea5529513a95636da3d6c30d3b6cae66aa81c4e170fa26abd148c8a650dd863140f5c"' }>
                                        <li class="link">
                                            <a href="injectables/CompanyService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CompanyService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/StationModule.html" data-type="entity-link" >StationModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-StationModule-09a0d9b4d14d84cc3d99dfb141fdb2993d8d5e1195247018679a2b12739e4ca2b2ac379704316c4642cccdaef80888b44218180021d8afd0d036956c4b2ef7cf"' : 'data-bs-target="#xs-injectables-links-module-StationModule-09a0d9b4d14d84cc3d99dfb141fdb2993d8d5e1195247018679a2b12739e4ca2b2ac379704316c4642cccdaef80888b44218180021d8afd0d036956c4b2ef7cf"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-StationModule-09a0d9b4d14d84cc3d99dfb141fdb2993d8d5e1195247018679a2b12739e4ca2b2ac379704316c4642cccdaef80888b44218180021d8afd0d036956c4b2ef7cf"' :
                                        'id="xs-injectables-links-module-StationModule-09a0d9b4d14d84cc3d99dfb141fdb2993d8d5e1195247018679a2b12739e4ca2b2ac379704316c4642cccdaef80888b44218180021d8afd0d036956c4b2ef7cf"' }>
                                        <li class="link">
                                            <a href="injectables/StationService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#entities-links"' :
                                'data-bs-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Company.html" data-type="entity-link" >Company</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Station.html" data-type="entity-link" >Station</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Company.html" data-type="entity-link" >Company</a>
                            </li>
                            <li class="link">
                                <a href="classes/CompanyCollectionOutput.html" data-type="entity-link" >CompanyCollectionOutput</a>
                            </li>
                            <li class="link">
                                <a href="classes/CompanyMutationsResolver.html" data-type="entity-link" >CompanyMutationsResolver</a>
                            </li>
                            <li class="link">
                                <a href="classes/CompanyOutput.html" data-type="entity-link" >CompanyOutput</a>
                            </li>
                            <li class="link">
                                <a href="classes/CompanyQueriesResolver.html" data-type="entity-link" >CompanyQueriesResolver</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateCompanyAndStationMigration1708343648496.html" data-type="entity-link" >CreateCompanyAndStationMigration1708343648496</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateCompanyInput.html" data-type="entity-link" >CreateCompanyInput</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateStationInput.html" data-type="entity-link" >CreateStationInput</a>
                            </li>
                            <li class="link">
                                <a href="classes/EnvironmentVariables.html" data-type="entity-link" >EnvironmentVariables</a>
                            </li>
                            <li class="link">
                                <a href="classes/StationCollectionOutput.html" data-type="entity-link" >StationCollectionOutput</a>
                            </li>
                            <li class="link">
                                <a href="classes/StationMutationsResolver.html" data-type="entity-link" >StationMutationsResolver</a>
                            </li>
                            <li class="link">
                                <a href="classes/StationOutput.html" data-type="entity-link" >StationOutput</a>
                            </li>
                            <li class="link">
                                <a href="classes/StationQueriesResolver.html" data-type="entity-link" >StationQueriesResolver</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateCompanyInput.html" data-type="entity-link" >UpdateCompanyInput</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateStationInput.html" data-type="entity-link" >UpdateStationInput</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#interfaces-links"' :
                            'data-bs-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/DatabaseConfig.html" data-type="entity-link" >DatabaseConfig</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});