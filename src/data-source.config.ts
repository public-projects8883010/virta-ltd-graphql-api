import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { DatabaseConfig } from 'src/config/db.config';
import * as dotenv from 'dotenv';

dotenv.config();

const configService = new ConfigService<DatabaseConfig>();

export const dataSourceConfig = new DataSource({
  type: 'postgres',
  host: configService.get('POSTGRES_HOST'),
  port: configService.get<number>('POSTGRES_PORT'),
  username: configService.get('POSTGRES_USER'),
  password: configService.get('POSTGRES_PASSWORD'),
  database: configService.get('POSTGRES_DB'),
  migrations: ['dist/**/migrations/*{.ts,.js}'],
  entities: ['dist/**/*.entity{.ts,.js}'],
});
