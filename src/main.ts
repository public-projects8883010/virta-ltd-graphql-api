import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import * as path from 'path';
import { ConfigService } from '@nestjs/config';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const logger = new Logger('InstanceLoader');
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const port: number = configService.get<number>('APP_PORT');

  // Serve Compodoc-generated documentation
  app.use('/docs', express.static(path.join(__dirname, '../documentation')));

  // Coverage for unit tests.
  app.use(
    '/coverage',
    express.static(path.join(__dirname, '../coverage/lcov-report')),
  );

  await app.listen(port);

  logger.log(`App started on port ${port}`);
}
bootstrap();
