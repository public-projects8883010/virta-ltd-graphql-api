import { Module } from '@nestjs/common';
import { CompanyModule } from './features/company/company.module';
import { StationModule } from './features/station/station.module';
import { ApolloDriverConfig, ApolloDriver } from '@nestjs/apollo';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { validate } from 'class-validator';
import dbConfig from './config/db.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormConfig } from './config/typeorm.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      validate,
      cache: true,
      isGlobal: true,
      load: [dbConfig],
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      playground: {
        workspaceName: 'virta-ltd',
        title: 'virta-ltd-playground',
      },
      path: '/',
      autoSchemaFile: true,
      introspection: true,
    }),
    TypeOrmModule.forRootAsync(typeormConfig),
    CompanyModule,
    StationModule,
  ],
})
export class AppModule {}
