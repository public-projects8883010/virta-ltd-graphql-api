import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCompanyAndStationMigration1708343648496
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "company" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, "parentId" uuid, CONSTRAINT "PK_056f7854a7afdba7cbd6d45fc20" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "station" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" text NOT NULL, "latitude" text NOT NULL, "longitude" text NOT NULL, "company_id" uuid NOT NULL, CONSTRAINT "PK_cad1b3e7182ef8df1057b82f6aa" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "company_closure_closure" ("parent_company_id" uuid NOT NULL, "child_company_id" uuid NOT NULL, CONSTRAINT "PK_0510d8181cbbd39dae28f740eab" PRIMARY KEY ("parent_company_id", "child_company_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_96c52e59072a9aed7defa37a9c" ON "company_closure_closure" ("parent_company_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_dff168cb6ba92baaab9e8b6793" ON "company_closure_closure" ("child_company_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "company" ADD CONSTRAINT "FK_58d280bd19fd40abeba89a59eb9" FOREIGN KEY ("parentId") REFERENCES "company"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "station" ADD CONSTRAINT "FK_5dcb2bfc0182146cf91d3752813" FOREIGN KEY ("company_id") REFERENCES "company"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "company_closure_closure" ADD CONSTRAINT "FK_96c52e59072a9aed7defa37a9cb" FOREIGN KEY ("parent_company_id") REFERENCES "company"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "company_closure_closure" ADD CONSTRAINT "FK_dff168cb6ba92baaab9e8b67935" FOREIGN KEY ("child_company_id") REFERENCES "company"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "company_closure_closure" DROP CONSTRAINT "FK_dff168cb6ba92baaab9e8b67935"`,
    );
    await queryRunner.query(
      `ALTER TABLE "company_closure_closure" DROP CONSTRAINT "FK_96c52e59072a9aed7defa37a9cb"`,
    );
    await queryRunner.query(
      `ALTER TABLE "station" DROP CONSTRAINT "FK_5dcb2bfc0182146cf91d3752813"`,
    );
    await queryRunner.query(
      `ALTER TABLE "company" DROP CONSTRAINT "FK_58d280bd19fd40abeba89a59eb9"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_dff168cb6ba92baaab9e8b6793"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_96c52e59072a9aed7defa37a9c"`,
    );
    await queryRunner.query(`DROP TABLE "company_closure_closure"`);
    await queryRunner.query(`DROP TABLE "station"`);
    await queryRunner.query(`DROP TABLE "company"`);
  }
}
