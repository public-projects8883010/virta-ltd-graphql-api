import { Args, Query, Resolver } from '@nestjs/graphql';
import { CompanyCollectionOutput } from '../dto/company-collection.output';
import { CompanyOutput } from '../dto/company.output';
import { CompanyService } from '../services/company.service';

@Resolver(() => CompanyOutput)
export class CompanyQueriesResolver {
  constructor(private readonly companyService: CompanyService) {}
  @Query(() => CompanyCollectionOutput, { name: 'companies' })
  async getAllCompanies(): Promise<CompanyCollectionOutput> {
    return await this.companyService.findAll();
  }

  @Query(() => CompanyOutput, {
    name: 'companyById',
    nullable: true,
  })
  findOneById(
    @Args('companyId', { type: () => String })
    companyId: string,
  ) {
    return this.companyService.findOneBy({ id: companyId });
  }

  @Query(() => CompanyCollectionOutput, {
    name: 'companies',
    nullable: true,
  })
  findAllCompanies() {
    return this.companyService.findAll();
  }

  @Query(() => CompanyCollectionOutput, {
    name: 'findCompaniesWithTreeStructure',
    nullable: true,
  })
  findCompaniesUnderTreeStructure(
    @Args('depth', { type: () => Number })
    depth: number,
  ) {
    return this.companyService.findCompaniesUnderTreeStructure(depth);
  }
}
