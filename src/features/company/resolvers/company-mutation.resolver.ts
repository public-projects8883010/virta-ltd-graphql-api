import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { CompanyOutput } from '../dto/company.output';
import { CreateCompanyInput } from '../dto/create-company.input';
import { UpdateCompanyInput } from '../dto/update-company.input';
import { CompanyService } from '../services/company.service';

@Resolver(() => CompanyOutput)
export class CompanyMutationsResolver {
  constructor(private readonly companyService: CompanyService) {}

  @Mutation(() => CompanyOutput, { name: 'createCompany' })
  create(
    @Args('input')
    createCompanyInput: CreateCompanyInput,
  ): Promise<CompanyOutput> {
    return this.companyService.create(createCompanyInput);
  }

  @Mutation(() => CompanyOutput, { name: 'updateCompany' })
  update(
    @Args('id', { type: () => String }) id: string,
    @Args('input')
    updateCompanyInput: UpdateCompanyInput,
  ): Promise<CompanyOutput> {
    return this.companyService.update(id, updateCompanyInput);
  }
}
