import { Station } from '../../station/entities/station.entity';
import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';

@Tree('closure-table', {
  closureTableName: 'company_closure',
  ancestorColumnName: (column) => 'parent_company_' + column.propertyName,
  descendantColumnName: (column) => 'child_company_' + column.propertyName,
})
@Entity('company')
export class Company {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text', { name: 'name', nullable: false })
  name: string;

  @TreeChildren()
  children: Company[];

  @TreeParent()
  parent: Company;

  @OneToMany(() => Station, (station) => station.company)
  stations: Station[];
}
