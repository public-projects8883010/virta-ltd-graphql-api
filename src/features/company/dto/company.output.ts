import { ObjectType, Field, ID } from '@nestjs/graphql';
import { StationOutput } from '../../station/dto/station.output';

@ObjectType()
export class CompanyOutput {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  name: string;

  @Field(() => CompanyOutput, { nullable: true })
  parent?: CompanyOutput; // Represents the parent company

  @Field(() => [CompanyOutput], { nullable: true })
  children?: CompanyOutput[]; // Represents the children companies

  @Field(() => [StationOutput], { nullable: true })
  stations?: StationOutput[];
}
