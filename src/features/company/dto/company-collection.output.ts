import { Field, ObjectType } from '@nestjs/graphql';
import { CompanyOutput } from './company.output';

@ObjectType()
export class CompanyCollectionOutput {
  @Field(() => [CompanyOutput], {
    defaultValue: [],
  })
  items: CompanyOutput[];

  @Field(() => Number, {
    defaultValue: 0,
  })
  count: number;
}
