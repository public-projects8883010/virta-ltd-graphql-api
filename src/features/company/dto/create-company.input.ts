import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateCompanyInput {
  @Field(() => String)
  name: string;

  @Field(() => ID, { nullable: true })
  parentId?: string;
}
