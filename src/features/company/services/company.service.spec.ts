import { EntityManager } from 'typeorm';
import { CreateCompanyInput } from '../dto/create-company.input';
import { CompanyService } from './company.service';
import { Test, TestingModule } from '@nestjs/testing';
import { createMock } from '@golevelup/ts-jest';
import { Company } from '../entities/company.entity';

describe('CompanyService', () => {
  const id = 'testId';
  const childId = 'childId';

  const input: CreateCompanyInput = {
    name: 'Company Name',
  };

  const entity = {
    id: id,
    ...input,
  };

  // child mock
  const childCompany: CreateCompanyInput = {
    parentId: id,
    name: 'Child Company',
  };

  const childEntity = {
    id: childId,
    ...childCompany,
  };

  let service: CompanyService;
  let entityManagerMock: EntityManager;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CompanyService,
        {
          provide: EntityManager,
          useValue: createMock<EntityManager>(),
        },
      ],
    }).compile();

    service = module.get(CompanyService);
    entityManagerMock = module.get(EntityManager);
  });

  /**
   * Testing if creating a new company is working as expected, without parent
   * Testing if creating a new company is working as expected, with parent
   * Testing Error handling both with or without parent
   */
  it('should call create company and save it successfully without parent', async () => {
    await service.create(input);

    expect(entityManagerMock.create).toHaveBeenCalledWith(Company);
    expect(entityManagerMock.save).toHaveBeenCalledWith(entity);
  });

  it('should call create company and save it successfully with parent', async () => {
    (entityManagerMock.findOneBy as jest.Mock).mockImplementation(() => entity);

    await service.create(childCompany);

    expect(entityManagerMock.create).toHaveBeenCalledWith(Company);
    expect(entityManagerMock.save).toHaveBeenCalledWith(childEntity);
  });

  it('should handle error during Company creation, not finding the parent', async () => {
    const input: CreateCompanyInput = {
      name: 'TestCompany',
      parentId: 'test',
    };
    const error = new Error('Company parent not found');
    (entityManagerMock.findOneBy as jest.Mock).mockImplementation(() => {
      throw error;
    });

    await expect(service.create(input)).rejects.toThrow(
      `Cannot find parent company: ${error.message}`,
    );
  });

  it('should handle error during Company creation with parent', async () => {
    const input: CreateCompanyInput = {
      name: 'TestCompany',
    };
    const error = new Error('Error at creation');
    (entityManagerMock.save as jest.Mock).mockImplementation(() => {
      throw error;
    });

    await expect(service.create(input)).rejects.toThrow(
      `Cannot create company: ${error.message}`,
    );
  });

  /**
   * Testing if updating a new company is working as expected, without parent
   * Testing if updating a new company is working as expected, with parent
   * Testing Error handling both with or without parent
   */
  it('should call update company and save it successfully without parent', async () => {
    (entityManagerMock.findOneBy as jest.Mock).mockImplementation(() => entity);

    await service.update(id, input);

    expect(entityManagerMock.findOneBy).toHaveBeenCalledWith(Company, {
      id: id,
    });
    expect(entityManagerMock.save).toHaveBeenCalledWith(entity);
  });

  it('should call update company and save it successfully with parent', async () => {
    (entityManagerMock.findOneBy as jest.Mock).mockResolvedValueOnce(
      childEntity,
    );
    (entityManagerMock.findOneBy as jest.Mock).mockResolvedValueOnce(entity);

    await service.update(childId, childCompany);

    expect(entityManagerMock.findOneBy).toHaveBeenCalledTimes(2);
    expect(entityManagerMock.findOneBy).toHaveBeenCalledWith(Company, {
      id: childId,
    });
    expect(entityManagerMock.findOneBy).toHaveBeenCalledWith(Company, { id });
    expect(entityManagerMock.save).toHaveBeenCalledWith(childEntity);
  });

  it('should handle error during Company find one by', async () => {
    const error = new Error(`Company with id ${id} not found`);
    (entityManagerMock.findOneBy as jest.Mock).mockImplementation(() => {});

    await expect(service.update(id, input)).rejects.toThrow(error);
  });

  it('should handle error during Company find one by', async () => {
    const error = new Error(`Company with id ${id} not found`);
    (entityManagerMock.findOneBy as jest.Mock).mockImplementation(() => {});

    await expect(service.update(id, input)).rejects.toThrow(error);
  });
});
