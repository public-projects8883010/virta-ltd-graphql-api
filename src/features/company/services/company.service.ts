import { Company } from '../entities/company.entity';
import { UpdateCompanyInput } from '../dto/update-company.input';
import { Injectable } from '@nestjs/common';
import { EntityManager, FindOptionsWhere } from 'typeorm';
import { assignDefined } from '../../../utils/assignDef';
import { CompanyCollectionOutput } from '../dto/company-collection.output';
import { InjectEntityManager } from '@nestjs/typeorm';
import { CreateCompanyInput } from '../dto/create-company.input';
import { CompanyOutput } from '../dto/company.output';
@Injectable()
export class CompanyService {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  async create(companyCreateInput: CreateCompanyInput): Promise<CompanyOutput> {
    const companyCreated = this.entityManager.create(Company);

    if (companyCreateInput.parentId) {
      try {
        const parentCompany = await this.entityManager.findOneBy(Company, {
          id: companyCreateInput.parentId,
        });
        companyCreated.parent = parentCompany;
      } catch (e) {
        throw new Error(`Cannot find parent company: ${e.message}`);
      }
    }

    assignDefined(companyCreated, companyCreateInput);

    try {
      return await this.entityManager.save(companyCreated);
    } catch (e) {
      throw new Error(`Cannot create company: ${e.message}`);
    }
  }

  async update(
    companyId: string,
    companyUpdateInput: UpdateCompanyInput,
  ): Promise<CompanyOutput> {
    const company = await this.entityManager.findOneBy(Company, {
      id: companyId,
    });
    Company;

    if (!company) {
      throw new Error(`Company with id ${companyId} not found`);
    }

    if (companyUpdateInput.parentId) {
      try {
        const parentCompany = await this.entityManager.findOneBy(Company, {
          id: companyUpdateInput.parentId,
        });
        company.parent = parentCompany;
      } catch (e) {
        throw new Error(`Cannot find parent company: ${e.message}`);
      }
    }

    assignDefined(company, companyUpdateInput);

    try {
      return await this.entityManager.save(company);
    } catch (e) {
      throw new Error(`Cannot create company: ${e.message}`);
    }
  }

  async findAll(): Promise<CompanyCollectionOutput> {
    const items = (await this.entityManager.find(Company, {
      relations: ['parent', 'children', 'stations'],
    })) as unknown as CompanyOutput[];
    const count = await this.entityManager.count(Company);

    return { items, count };
  }

  async findOneBy(where: FindOptionsWhere<Company>): Promise<CompanyOutput> {
    const company = await this.entityManager.findOne(Company, {
      where: where,
      relations: ['parent', 'children', 'stations'],
    });
    return company;
  }

  async findCompaniesUnderTreeStructure(
    depth: number,
  ): Promise<CompanyCollectionOutput> {
    const items = await this.entityManager
      .getTreeRepository(Company)
      .findTrees({
        depth: depth,
        relations: ['children', 'parent', 'stations'],
      });
    const count = await this.entityManager.count(Company);

    return { items, count };
  }
}
