import { Module } from '@nestjs/common';
import { CompanyService } from './services/company.service';
import { CompanyMutationsResolver } from './resolvers/company-mutation.resolver';
import { CompanyQueriesResolver } from './resolvers/company-queries.resolver';

@Module({
  providers: [CompanyService, CompanyMutationsResolver, CompanyQueriesResolver],
})
export class CompanyModule {}
