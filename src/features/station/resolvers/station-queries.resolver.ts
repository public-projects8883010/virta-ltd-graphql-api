import { Args, Query, Resolver } from '@nestjs/graphql';
import { StationOutput } from '../dto/station.output';
import { StationCollectionOutput } from '../dto/station-collection.output';
import { StationService } from '../services/station.service';

@Resolver(() => StationOutput)
export class StationQueriesResolver {
  constructor(private readonly stationService: StationService) {}
  @Query(() => StationCollectionOutput, { name: 'stations' })
  async getAllStations(): Promise<StationCollectionOutput> {
    return await this.stationService.findAll();
  }

  @Query(() => StationCollectionOutput, { name: 'stationsWithinRadius' })
  async getAllStationsWithinRadius(
    @Args('companyId', { type: () => String, nullable: true })
    companyId: string,
    @Args('latitude', { type: () => Number })
    latitude: number,
    @Args('longitude', { type: () => Number })
    longitude: number,
    @Args('radiusInKm', { type: () => Number })
    radiusInKm: number,
  ): Promise<StationCollectionOutput> {
    return await this.stationService.findStationsWithinRadius(
      companyId,
      latitude,
      longitude,
      radiusInKm,
    );
  }
}
