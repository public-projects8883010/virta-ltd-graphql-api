import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { StationOutput } from '../dto/station.output';
import { CreateStationInput } from '../dto/create-station.input';
import { StationService } from '../services/station.service';
import { UpdateStationInput } from '../dto/update-station';

@Resolver(() => StationOutput)
export class StationMutationsResolver {
  constructor(private readonly stationService: StationService) {}

  @Mutation(() => StationOutput, { name: 'createStation' })
  create(
    @Args('input')
    createStationInput: CreateStationInput,
  ): Promise<StationOutput> {
    return this.stationService.create(createStationInput);
  }

  @Mutation(() => StationOutput, { name: 'updateStation' })
  update(
    @Args('id', { type: () => String }) id: string,
    @Args('input')
    updateStationInput: UpdateStationInput,
  ): Promise<StationOutput> {
    return this.stationService.update(id, updateStationInput);
  }
}
