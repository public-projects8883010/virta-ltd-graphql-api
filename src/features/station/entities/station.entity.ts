import { Company } from 'src/features/company/entities/company.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('station')
export class Station {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text', { name: 'name', nullable: false })
  name: string;

  @Column('text', { name: 'latitude', nullable: false })
  latitude: string;

  @Column('text', { name: 'longitude', nullable: false })
  longitude: string;

  @Column({
    type: 'uuid',
    name: 'company_id',
  })
  companyId: string;

  @ManyToOne(() => Company, (company) => company.stations, { nullable: true })
  @JoinColumn({
    name: 'company_id',
    referencedColumnName: 'id',
  })
  company: Company;
}
