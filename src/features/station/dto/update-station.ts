import { InputType } from '@nestjs/graphql';
import { CreateStationInput } from './create-station.input';

@InputType()
export class UpdateStationInput extends CreateStationInput {}
