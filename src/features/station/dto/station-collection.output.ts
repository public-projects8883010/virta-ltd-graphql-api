import { Field, ObjectType } from '@nestjs/graphql';
import { StationOutput } from './station.output';

@ObjectType()
export class StationCollectionOutput {
  @Field(() => [StationOutput], {
    defaultValue: [],
  })
  items: StationOutput[];

  @Field(() => Number, {
    defaultValue: 0,
  })
  count: number;
}
