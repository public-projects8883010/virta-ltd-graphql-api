import { Field, ID, ObjectType } from '@nestjs/graphql';
import { CompanyOutput } from 'src/features/company/dto/company.output';

@ObjectType()
export class StationOutput {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  name: string;

  @Field(() => String)
  latitude: string;

  @Field(() => String)
  longitude: string;

  @Field(() => String)
  companyId: string;

  @Field(() => CompanyOutput, { nullable: true })
  company?: CompanyOutput; // Represents the parent company
}
