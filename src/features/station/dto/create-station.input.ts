import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class CreateStationInput {
  @Field(() => String)
  name: string;

  @Field(() => String)
  latitude: string;

  @Field(() => String)
  longitude: string;

  @Field(() => ID, { nullable: true })
  companyId?: string; // Represents the parent company
}
