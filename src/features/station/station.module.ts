import { Module } from '@nestjs/common';
import { StationService } from './services/station.service';
import { StationMutationsResolver } from './resolvers/station-mutations.resolver';
import { StationQueriesResolver } from './resolvers/station-queries.resolver';

@Module({
  providers: [StationService, StationMutationsResolver, StationQueriesResolver],
})
export class StationModule {}
