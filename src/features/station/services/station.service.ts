import { UpdateStationInput } from '../dto/update-station';
import { Station } from '../entities/station.entity';
import { assignDefined } from '../../../utils/assignDef';
import { EntityManager, FindOptionsWhere } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { CreateStationInput } from '../dto/create-station.input';
import { StationCollectionOutput } from '../dto/station-collection.output';
import { StationOutput } from '../dto/station.output';
import * as haversine from 'haversine-distance';

@Injectable()
export class StationService {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  async create(stationCreateInput: CreateStationInput): Promise<StationOutput> {
    const stationCreated = this.entityManager.create(Station);

    assignDefined(stationCreated, stationCreateInput);

    return (await this.entityManager.save(
      stationCreated,
    )) as unknown as StationOutput;
  }

  async update(
    stationId: string,
    stationUpdateInput: UpdateStationInput,
  ): Promise<StationOutput> {
    const station = await this.entityManager.findOneBy(Station, {
      id: stationId,
    });

    if (!station) {
      throw new Error(`Station with id ${stationId} not found`);
    }

    assignDefined(station, stationUpdateInput);

    return (await this.entityManager.save(station)) as unknown as StationOutput;
  }

  async findAll(): Promise<StationCollectionOutput> {
    const items = (await this.entityManager.find(Station, {
      relations: ['company'],
    })) as unknown as StationOutput[];
    const count = await this.entityManager.count(Station);

    return { items, count };
  }

  findOneBy(where: FindOptionsWhere<Station>): Promise<Station> {
    return this.entityManager.findOneBy(Station, where);
  }

  findAllBy(where: FindOptionsWhere<Station>): Promise<Station[]> {
    return this.entityManager.find(Station, {
      where: where,
    });
  }

  async findStationsWithinRadius(
    companyId: string | null,
    latitude: number,
    longitude: number,
    radiusInKm: number,
  ): Promise<StationCollectionOutput> {
    const whereCondition: any = {};

    // Check if companyId is provided
    if (companyId) {
      whereCondition.companyId = companyId;
    }

    const stations = await this.entityManager.find(Station, {
      where: whereCondition,
      relations: ['company'], // Ensure we load company relation
    });

    // Calculate distances and filter within the radius
    const stationsWithinRadius = stations.filter((station) => {
      const distanceInM = haversine(
        { lat: latitude, lon: longitude },
        {
          lat: parseFloat(station.latitude),
          lon: parseFloat(station.longitude),
        },
      );
      const distanceInKm = distanceInM / 1000;

      return distanceInKm <= radiusInKm;
    });

    console.log('stationsWithinRadius = ', stationsWithinRadius);

    // Sort by distance
    stationsWithinRadius.sort((a, b) => {
      const distanceA = haversine(
        { lat: latitude, lon: longitude },
        {
          lat: parseFloat(a.latitude),
          lon: parseFloat(a.longitude),
        },
      );

      const distanceB = haversine(
        { lat: latitude, lon: longitude },
        {
          lat: parseFloat(b.latitude),
          lon: parseFloat(b.longitude),
        },
      );

      return distanceA - distanceB;
    });

    return { items: stationsWithinRadius, count: stationsWithinRadius.length };
  }
}
