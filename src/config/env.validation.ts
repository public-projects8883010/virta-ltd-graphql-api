import { plainToInstance } from 'class-transformer';
import {
  IsBoolean,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  validateSync,
} from 'class-validator';

export enum Environment {
  Development = 'development',
  Production = 'production',
  Sales = 'sales',
  Test = 'test',
}

// We use MongoDB, this version of enum with 3 most used DBs it's just a convention. Can be deleted in the Future.
enum DbTypes {
  Mongodb = 'mongodb',
  MySQL = 'mysql',
  Postgres = 'postgres',
}

class EnvironmentVariables {
  @IsString()
  @IsNotEmpty()
  APP_LOCATION: string;

  @IsNumber()
  @IsNotEmpty()
  APP_PORT: number;

  @IsNotEmpty()
  @IsEnum(Environment)
  NODE_ENV: Environment;

  @IsString()
  @IsNotEmpty()
  POSTGRES_HOST: string;

  @IsNumber()
  @IsNotEmpty()
  POSTGRES_PORT: number;

  @IsString()
  @IsNotEmpty()
  POSTGRES_USER: string;

  @IsString()
  @IsNotEmpty()
  POSTGRES_PASSWORD: string;

  @IsString()
  @IsNotEmpty()
  POSTGRES_DB: string;

  @IsString()
  @IsNotEmpty()
  POSTGRES_SUPERSET_DB: string;

  @IsBoolean()
  POSTGRES_LOGGING_LEVEL: boolean;

  @IsNotEmpty()
  @IsEnum(DbTypes)
  DB_TYPE: string;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToInstance(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
