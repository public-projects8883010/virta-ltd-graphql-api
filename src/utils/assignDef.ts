import { assignWith } from 'lodash';

export function assignDefined<T>(target: T, ...sources: Partial<T>[]): T {
  for (const source of sources) {
    assignWith(
      target,
      source,
      (targetValue, sourceValue) =>
        (sourceValue === undefined ? targetValue : sourceValue) as unknown,
    );
  }

  return target;
}
