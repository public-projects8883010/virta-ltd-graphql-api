# Use the official Node.js image as the base image
FROM node:20-alpine3.18

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the entire application to the working directory
COPY . .

# Expose the port that your Nest.js application will run on
EXPOSE 3000

# Command to run your Nest.js application
CMD ["npm", "run", "start:dev"]