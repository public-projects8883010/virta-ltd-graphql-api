<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

  <p align="center">Build with <b>Nest.js</b> A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

# Dream Labs Invoicing Test

Hello & welcome!

This is the submited application for the Company with stations test asked by Virta LTD.

Keep in mind that this has an example of everything, it's not a production ready (i.e it doesn't have all tests written and also soft deletion, I can provide that if requested, no problem).

Have Fun!

## Description

The env vars for POSTGRES_HOST & POSTGRES_PORT are for local npm commands and `npm run start:dev` only.

To route `/` is for using the playground, please refer to it's schema & docs on how to use the app (it's on the right).

Simply run following commands

## Docker only for postgres

### Run with docker postgres

```bash
# to run in containers the full app with db run
$ docker-compose up

# to run only the postgres please run the following
$ docker-compose up virta-ltd-db
```

### Installation

```bash
# to install everything
$ npm install
```

### Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

### Test - unit only

```bash
# unit tests
$ npm run test
```

## How to access & use

- To access docs: [click here](http://localhost:3020/docs).
- To access code coverage: [click here](http://localhost:3020/coverage).
- To access graphql playground: [click here](http://localhost:3020)

```JSON

# for example to get all stations with companies here is a query
query {
 stations {
    items {
      id
      company {
        id
        name
        children {
          name
          parent {
            id
            name
          }
        }
      }
    }
  }
}

# or for example adding a station: 

mutation {
  createStation(
    input: {
      name: "Sydney, Australia"
      latitude: "40.7128"
      longitude: "-74.0060"
      companyId: "550609be-5834-440f-afc6-489b5b33c611"
    }
  ) {
    id
    name
  }
}

# or get the stations in radius
# company id is not needed, only if you want to filter by companies

query {
  stationsWithinRadius(
    companyId: "550609be-5834-440f-afc6-489b5b33c611"
    radiusInKm: 10
    latitude:40.730610
    longitude:-73.935242
  ) {
    items {
      id
      company {
        id
        name
        children {
          name
          parent {
            id
            name
          }
        }
      }
    }
  }
}

# for more look at the docs and schema on the playground
```

## Tests

For how I do tests, look at customer.service.spec.ts
It's a unit test as example, also access /coverage route to see the coverage for company.service.ts. If needed I can provide tests for stations as well, just let me know.

## Support

Done with NEST.js
Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Abrudan Eduard](https://www.linkedin.com/in/abrudan-eduard/)
- Website - [https://elisium-tech.com/](https://elisium-tech.com/)

## License

Nest is [MIT licensed](LICENSE).
